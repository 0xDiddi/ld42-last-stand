class Unit {
    constructor(scene, type) {
        this.scene = scene;

        let atlas = "";
        if (type.startsWith("person_")) atlas = "atlas_td";
        else if (type.startsWith("tank_")) {
            atlas = "atlas_tanks";
            this.isTank = true;
        }

        this.pos = new Phaser.Math.Vector2(
            this.scene.path["spawn"].x,
            this.scene.path["spawn"].y
        );
        this.distanceTravelled = 0;

        this.nextIndex = 0;
        this.nextNode = this.scene.path[this.nextIndex];

        this.sprite = this.scene.add.sprite(
            this.pos.x,
            this.pos.y,
            R.image[atlas],
            R[atlas][type]
        );

        switch (type) {
            case "person_1":
                this.speed = 30;
                this.health = 10;
                this.value = 15;
                this.timeout = 2000;
                break;
            case "person_2":
                this.speed = 25;
                this.health = 25;
                this.value = 20;
                this.timeout = 2000;
                break;
            case "tank_1":
                this.speed = 15;
                this.health = 100;
                this.value = 25;
                this.timeout = 4000;
                break;
            case "tank_2":
                this.speed = 15;
                this.health = 200;
                this.value = 50;
                this.timeout = 5000;
                break;
            case "tank_3":
                this.speed = 10;
                this.health = 300;
                this.value = 75;
                this.timeout = 6000;
                break;
        }
    }

    update(time, delta) {
        if (this.health < 0) {
            this.dead = true;
            this.scene.currency += this.value;
            this.sprite.destroy();
        }

        let dist = this.nextNode.clone().subtract(this.pos);
        let b4 = dist.clone();
        dist.normalize();
        dist.multiply(new Phaser.Math.Vector2(this.speed * delta));
        if (b4.length() < dist.length()) dist = b4;

        this.pos.add(dist);
        this.sprite.x = this.pos.x;
        this.sprite.y = this.pos.y;

        this.distanceTravelled += dist.length();

        this.sprite.setAngle(dist.angleDegrees() - (this.isTank ? 90 : 0));

        if (this.pos.equals(this.nextNode)) {
            this.nextIndex++;
            if (this.scene.path.hasOwnProperty(this.nextIndex))
                this.nextNode = this.scene.path[this.nextIndex];
            else this.nextNode = this.scene.path["finish"];
        }
    }
}
