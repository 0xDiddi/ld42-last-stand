let game = null;

const init = function() {
    let config = {
        // Tilemaps aren't rendering in canvas mode
        // type: Phaser.CANVAS,
        width: 640,
        height: 480,
        pixelArt: true
    };

    game = new Phaser.Game(config);

    game.scene.add(R.state.preload, Preloader);
    game.scene.add(R.state.mainmenu, MainMenu);
    game.scene.add(R.state.maingame, MainGame);

    game.scene.start(R.state.preload);
};

const R = {
    font: {
        mario: 'mario'
    },
    state: {
        preload: 'preload',
        mainmenu: 'mainmenu',
        maingame: 'maingame'
    },
    image: {
        tileset_1: 'tile_1',
        tileset_2: 'tile_2',

        atlas_rts: 'rts',
        atlas_tanks: 'tanks',
        atlas_td: 'td',

        header: 'header',
        particle: 'particle'
    },
    atlas_td: {
        base_1: 'towerDefense_tile181.png',
        gun_turret_1: 'towerDefense_tile249.png',
        gun_turret_2: 'towerDefense_tile250.png',
        rocket_turret_1: 'towerDefense_tile206.png',
        rocket_turret_2: 'towerDefense_tile205.png',
        person_1: 'towerDefense_tile245.png',
        person_2: 'towerDefense_tile248.png',
        rocket: 'towerDefense_tile251.png'
    },
    atlas_rts: {
        pylon_on: 'scifiStructure_13.png',
        pylon_off: 'scifiStructure_14.png'
    },
    atlas_tanks: {
        tank_1: 'tank_dark.png',
        tank_2: 'tank_darkLarge.png',
        tank_3: 'tank_huge.png',
        bullet: 'bulletBlue1_outline.png'
    }
};
