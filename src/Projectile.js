class Projectile {
    constructor(scene, type, direction, pos) {
        this.scene = scene;
        this.type = type;
        this.direction = direction;
        this.pos = pos;

        if (this.type === "bullet") {
            this.sprite = this.scene.add.sprite(
                this.pos.x,
                this.pos.y,
                R.image.atlas_tanks,
                R.atlas_tanks.bullet
            );
            this.speed = 150;
            this.damage = 1;
        } else if (this.type === "rocket") {
            this.sprite = this.scene.add.sprite(
                this.pos.x,
                this.pos.y,
                R.image.atlas_td,
                R.atlas_td.rocket
            );
            this.speed = 150;
            this.damage = 5;
        }

        this.sprite.setAngle(this.direction.angleDegrees() + 90);
    }

    update(time, delta) {
        if (this.done) return;

        let dist = this.direction
            .clone()
            .multiply(new Phaser.Math.Vector2(this.speed * delta));

        this.pos.add(dist);
        this.sprite.x = this.pos.x;
        this.sprite.y = this.pos.y;

        this.scene.enemies.forEach(e => {
            let a = e.sprite.getBounds();
            let b = this.sprite.getBounds();
            if (Phaser.Geom.Rectangle.Overlaps(a, b)) {
                e.health -= this.damage;
                this.done = true;
                this.sprite.destroy();
            }
        }, this);
    }
}
