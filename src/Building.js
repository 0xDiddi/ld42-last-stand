class Building {
    constructor(scene, mapObject) {
        this.scene = scene;
        this.x = mapObject.x + mapObject.width / 2;
        this.y = mapObject.y + mapObject.height / 2;
        this.type = mapObject.type;

        if (this.type === "building") {
            this.sprite = this.scene.add.sprite(
                this.x,
                this.y,
                R.image.atlas_td,
                R.atlas_td.base_1
            );
            this.sprite.setInteractive();

            this.sprite.on(
                "pointerup",
                function() {
                    if (this.scene.currency >= 50 && !this.turret) {
                        this.turret = new Turret(this.scene, this, "gun1");
                        this.scene.currency -= 50;
                    } else if (
                        this.scene.currency >= 50 &&
                        this.turret.type === "gun1"
                    ) {
                        this.turret.init("gun2");
                        this.scene.currency -= 50;
                    } else if (
                        this.scene.currency >= 75 &&
                        this.turret.type === "gun2"
                    ) {
                        this.turret.init("rocket1");
                        this.scene.currency -= 75;
                    } else if (
                        this.scene.currency >= 75 &&
                        this.turret.type === "rocket1"
                    ) {
                        this.turret.init("rocket2");
                        this.scene.currency -= 75;
                    }
                },
                this
            );

            this.pylon = mapObject.properties["pylon"];
            this.pylon_active = true;
        } else if (this.type === "pylon") {
            this.sprite = this.scene.add.sprite(
                this.x,
                this.y - 16,
                R.image.atlas_rts,
                R.atlas_rts.pylon_off
            );

            this.id = mapObject.properties["id"];

            this.particles = this.scene.add.particles(R.image.particle);
            this.emitter = this.particles.createEmitter({
                x: this.x,
                y: this.y - 26,
                radial: true,
                angle: { min: -80, max: -100 },
                speed: { min: 50, max: 100 },
                gravityY: 200,
                scale: { start: 0.3, end: 0 },
                lifespan: 1000,
                frequency: 150
            });

            this.powered = true;
        }
    }

    update(time, delta) {
        if (!this.powered && !this.pylon_active) return;

        if (this.type === "building") {
            this.pylon_active = true;
            this.scene.buildings.forEach(b => {
                if (b.type === "pylon" && b.id === this.pylon)
                    this.pylon_active = b.powered;
            }, this);

            if (this.turret && this.pylon_active)
                this.turret.update(time, delta);
            else if (this.turret)
                this.turret.projectiles.forEach(p => p.sprite.destroy());
            if (!this.pylon_active) this.sprite.disableInteractive();
        } else if (this.type === "pylon") {
            this.scene.enemies.forEach(e => {
                let a = e.sprite.getBounds();
                let b = this.sprite.getBounds();
                if (Phaser.Geom.Rectangle.Overlaps(a, b)) {
                    this.powered = false;
                    this.emitter.stop();
                }
            }, this);
        }
    }
}
