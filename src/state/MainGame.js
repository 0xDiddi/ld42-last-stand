class MainGame {
    preload() {
        this.load.tilemapTiledJSON("map", "assets/map_1.json");
    }

    create() {
        this.map = this.add.tilemap("map");
        let ts1 = this.map.addTilesetImage("tiles_1", R.image.tileset_1);
        this.cameras.main.setBounds(
            0,
            0,
            this.map.widthInPixels,
            this.map.heightInPixels
        );

        this.map.createStaticLayer("bg", ts1, 0, 0);

        // Create path
        this.path = {};

        this.map.getObjectLayer("path").objects.forEach(function(o) {
            var p = new Phaser.Math.Vector2(
                o.x + o.width / 2,
                o.y + o.height / 2
            );

            if (o.type === "waypoint") this.path[o.properties["id"]] = p;
            else this.path[o.type] = p;
        }, this);

        // Create buildings
        this.buildings = [];

        this.map.getObjectLayer("buildings").objects.forEach(function(o) {
            this.buildings.push(new Building(this, o));
        }, this);

        this.currency = this.map.properties["currency"];
        this.currencyDisplay = this.add.bitmapText(
            10,
            10,
            R.font.mario,
            "Money: " + this.currency + "$",
            30
        );
        this.currencyDisplay.setScrollFactor(0);
        this.currencyDisplay.depth = 1;

        this.currentWave = 1;
        this.waveDisplay = this.add.bitmapText(
            10,
            35,
            R.font.mario,
            "Current wave: " + this.currentWave,
            30
        );
        this.waveDisplay.setScrollFactor(0);
        this.waveDisplay.depth = 1;

        this.timeout = 2000;
        this.lastSpawn = 0;
        this.waves = [
            [{ type: "person_1", amount: 5 }],
            [
                { type: "person_2", amount: 4 },
                { type: "tank_1", amount: 1 },
                { type: "person_2", amount: 4 },
                { type: "tank_1", amount: 1 }
            ],
            [
                { type: "person_2", amount: 4 },
                { type: "tank_1", amount: 2 },
                { type: "tank_2", amount: 1 }
            ],
            [
                { type: "tank_1", amount: 2 },
                { type: "tank_2", amount: 2 },
                { type: "person_2", amount: 3 },
                { type: "tank_3", amount: 1 }
            ]
        ];
        this.enemies = [];

        let cursor = this.input.keyboard.createCursorKeys();
        let controlConfig = {
            camera: this.cameras.main,
            left: cursor.left,
            right: cursor.right,
            up: cursor.up,
            down: cursor.down,
            acceleration: 0.5,
            drag: 0.05,
            maxSpeed: 1.0
        };
        this.controls = new Phaser.Cameras.Controls.SmoothedKeyControl(
            controlConfig
        );
    }

    update(time, delta) {
        this.controls.update(delta);
        delta = delta / 1000;

        this.buildings.forEach(o => o.update(time, delta));

        this.currencyDisplay.text = "Money: " + this.currency + "$";

        this.enemies.forEach(e => e.update(time, delta));
        this.enemies = this.enemies.filter(e => !e.dead);

        if (time - this.lastSpawn > this.timeout && this.waves.length > 0) {
            this.lastSpawn = time;

            let a = this.waves[0][0];
            let u = new Unit(this, a.type);
            this.enemies.push(u);
            this.timeout = u.timeout;
            a.amount--;
            if (a.amount === 0) {
                this.waves[0].splice(0, 1);
                console.info();
            }
            if (this.waves[0].length === 0) {
                this.waves.splice(0, 1);
                this.timeout = 15000;
                this.currentWave++;
                this.waveDisplay.text = "Current wave: " + this.currentWave;
            }
        }
    }
}
