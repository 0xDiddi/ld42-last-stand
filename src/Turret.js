class Turret {
    constructor(scene, parent, type) {
        this.scene = scene;
        this.parent = parent;

        this.pos = new Phaser.Math.Vector2(parent.x, parent.y);

        this.init(type);

        this.angle = 0;

        this.projectiles = [];
    }

    init(type) {
        if (this.initialized) this.reset();
        this.type = type;

        if (this.type.startsWith("gun")) this.projectile = "bullet";
        else if (this.type.startsWith("rocket")) this.projectile = "rocket";

        if (this.type === "gun1") {
            this.sprite = this.scene.add.sprite(
                this.pos.x,
                this.pos.y,
                R.image.atlas_td,
                R.atlas_td.gun_turret_1
            );
            this.sprite.setOrigin(0.5, 0.6);
            this.range = 96;
            this.fireDelay = 500;
        } else if (this.type === "gun2") {
            this.sprite = this.scene.add.sprite(
                this.pos.x,
                this.pos.y,
                R.image.atlas_td,
                R.atlas_td.gun_turret_2
            );
            this.sprite.setOrigin(0.5, 0.6);
            this.range = 96;
            this.fireDelay = 250;
        } else if (this.type === "rocket1") {
            this.sprite = this.scene.add.sprite(
                this.pos.x,
                this.pos.y,
                R.image.atlas_td,
                R.atlas_td.rocket_turret_1
            );
            this.range = 128;
            this.fireDelay = 500;
        } else if (this.type === "rocket2") {
            this.sprite = this.scene.add.sprite(
                this.pos.x,
                this.pos.y,
                R.image.atlas_td,
                R.atlas_td.rocket_turret_2
            );
            this.range = 128;
            this.fireDelay = 250;
        }
        this.lastShot = 0;
        this.sprite.depth = 1;

        this.initialized = true;
    }

    reset() {
        this.sprite.destroy();
    }

    update(time, delta) {
        this.projectiles.forEach(p => p.update(time, delta));
        this.projectiles = this.projectiles.filter(e => !e.done);

        let enemies = this.scene.enemies.filter(e => {
            // Enemies in range
            let dist = e.pos.clone().subtract(this.pos);
            return dist.length() < this.range;
        }, this);

        if (enemies.length === 0) {
            this.angle++;
            return;
        }

        // the furthest enemy in range
        let target = enemies.reduce((prev, curr) =>
            prev.distanceTravelled > curr.distanceTravelled ? prev : curr
        );

        let dist = target.pos
            .clone()
            .subtract(this.pos)
            .normalize();
        this.angle = dist.angleDegrees() + 90;

        if (time - this.lastShot > this.fireDelay) {
            this.projectiles.push(
                new Projectile(
                    this.scene,
                    this.projectile,
                    dist,
                    this.pos.clone()
                )
            );
            this.lastShot = time;
        }
    }

    get angle() {
        return this._angle;
    }
    set angle(value) {
        this._angle = value;
        this.sprite.setAngle(value);
    }
}
